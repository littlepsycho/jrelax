package com.jrelax.web.codegen.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import com.jrelax.config.JRelaxCoreConfigHelper;
import com.jrelax.kit.FileKit;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 开发 - UI布局设计器
 * @author zengchao
 *
 */
@Controller
@RequestMapping("/open/dev/layout")
public class LayoutitController {
	private final String TPL = "open/dev/layoutit/";
	
	@RequestMapping(method={RequestMethod.GET, RequestMethod.POST})
	public String index(Model model){
		return TPL + "index";
	}
	/**
	 * 保存布局
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/download", method={RequestMethod.GET, RequestMethod.POST})
	public String saveLayout(HttpServletRequest request, Model model, String html){
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource(JRelaxCoreConfigHelper.getProperty("velocity.resourceLoaderPath") + "/open/dev/layoutit/template/content.html");
		try {
			String content = IOUtils.toString(resource.getInputStream(), "UTF-8");
			html = content.replace("#code", html);
			html = html.replaceAll("#Pager", "#pager");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		model.addAttribute("html", html);
		return TPL + "template/download";
	}
	
}
