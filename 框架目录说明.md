## 框架目录说明

* src/main/java 类文件目录
* resources 配置文件和模板目录
  * core-config.properties 项目配置
  * jdbc_mysql.properties 数据库连接配置文
  * plugins.xml 插件配置文件，用于启用／禁用插件、注入插件参数等
  * service-context.xml Spring配置文件
  * servlet-context.xml SpringMVC 配置文件
  * resources 静态资源目录
  * templates velocity模板目录